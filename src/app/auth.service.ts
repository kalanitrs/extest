import { Injectable } from '@angular/core';

import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user: Observable<firebase.User>;
  email:string;

  constructor(private firebaseAuth: AngularFireAuth) {
    this.user = firebaseAuth.authState;
   }

  login(email: string, password: string) {
    this.email = email;
    return this.firebaseAuth.auth.signInWithEmailAndPassword(email,password);
  }

  logOut() {
    return this.firebaseAuth.auth.signOut();
  }
}
