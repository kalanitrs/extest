import { Component, OnInit } from '@angular/core';

import { AuthService } from '../auth.service';
import { AngularFireList, AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {
  items = [];

  constructor(private authService: AuthService, private db: AngularFireDatabase) { }

  ngOnInit() {
    this.authService.user.subscribe(
      user => {
        this.db.list('/users/'+user.uid+'/items').snapshotChanges().subscribe(
          items => {
            this.items = [];
            items.forEach(
              item => {
                let single = item.payload.toJSON();
                single['key'] = item.key;
                this.items.push(single);
              }
            )
          }
        )
      }
    )
  }

}
