import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {
  @Input() data:any;

  price: number;
  name: string;
  doesExist: boolean;

  constructor() { }

  ngOnInit() {
    this.price = this.data.price;
    this.name = this.data.name;
  }
}
