import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  constructor(private router:Router, private authService:AuthService) { }

  ngOnInit() {
  }

  toItems() {
    this.router.navigate(['/items'])
  }

  toLogin() {
    this.router.navigate(['/login'])
  }

  logOut() {
    this.authService.logOut()
    .then(value => {
      this.router.navigate(['/login'])
    }).catch(err => {
      console.log(err)
    });
  }

}
